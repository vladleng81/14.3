
#include <iostream>
#include <iomanip>
#include <string>

int main()
{
    std::string jotaro("dio");

    std::cout << jotaro << "\n";

    std::cout << "size:";
    std::cout << jotaro.length() << "\n";

    std::cout << "first symbol:";
    std::cout << jotaro.at(0) << "\n";

    std::cout << "last symbol:";
    std::cout << jotaro.at(2) << "\n";
}